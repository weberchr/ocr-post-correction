import datetime
import os
import re

from utilities import progress


def main():
    starttime = datetime.datetime.now()

    # Pfade
    rootdir = './data/dta/dta_komplett_2017-09-01/simple'
    destinationdir = './data/dta/dta_komplett_txt'

    path, dirs, files = os.walk(rootdir).__next__()
    file_count = len(files)

    progress(0, file_count, '')

    for i, file in enumerate(files):
        dir = os.path.join(path, file)

        m = re.match(r"(?P<filename>.*)(?P<rest>\.tcf\.xml)", file)
        filename = m.group('filename') + '.txt'
        outdir = os.path.join(destinationdir, filename)
        outfile = open(outdir, mode='at', encoding='utf-8')

        intext = False
        with open(dir, encoding="utf-8") as f:
            for line in f:
                # Ende
                if re.search('</text>', line):
                    intext = False
                # Im Text
                if intext:
                    outfile.write(line)
                # Anfang
                if re.search('<text>', line):
                    intext = True

        outfile.close()
        progress(i + 1, file_count, '')

    # print program runtime
    endtime = datetime.datetime.now()
    print("\n", "Completed in", endtime - starttime)


if __name__ == '__main__':
    main()
