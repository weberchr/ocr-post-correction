import os
import sys
import string
import pickle
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

'''
Aufruf über:

python 2_CharNGramme.py
'''

def main():

    rootdir = 'data/dta/dta_komplett_txt'
    out = 'data/ngramme/chars'

    nList = [1, 2, 3]

    fileList = os.listdir(rootdir)
    fileList = ['/'.join([rootdir, fileList[i]]) for i in range(len(fileList))]

    min_f = 2



    for n in nList:
        print('Buchstaben', str(n) + '-Gramme bilden...')

        charSet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                   'u',
                   'v', 'w', 'x', 'y', 'z', 'ß', 'à', 'ä', 'æ', 'è', 'é', 'ö', 'ü', 'ſ', 'ͤ', 'ꝛ'}

        finalCharSet = set()

        if n > 1:
            for elem1 in charSet:
                for elem2 in charSet:
                    if n == 2:
                        finalCharSet.add(elem1 + elem2)
                    if n == 3:
                        for elem3 in charSet:
                            finalCharSet.add(elem1 + elem2 + elem3)

            charSet = finalCharSet


        cv = CountVectorizer(input='filename', ngram_range=(n, n), analyzer='char', vocabulary=charSet)

        X = cv.fit_transform(fileList).toarray()
        vocabulary = cv.get_feature_names()
        dist = np.sum(X, axis=0)

        maindict = {}


        print('Dictionary bilden...')

        for tag, count in zip(vocabulary, dist):
            if count >= min_f:
                maindict[tag] = count

        X, vocabulary, dist, charSet = 0, 0, 0, 0  # entlasten des Speichers

        print('Dictionary abspeichern...')
        # Abspeichern von N
        with open(out + '/' + str(n) + 'N-char-freq.txt', 'w') as f:
            f.write(str(sum(maindict.values())))  # Absolute Anzahl an Ngrammen
            f.write('\n')
            f.write(str(len(maindict)))  # Einzigartige Ngramme

        # Abspeichern des Dict
        with open(out + '/' + str(n) + 'Gram-char-freq.pkl', 'wb') as f:
            pickle.dump(dict(maindict), f, protocol=pickle.HIGHEST_PROTOCOL)


    print('Abgeschlossen')

    os._exit(0)  # Um Garbage Collector zu umgehen, Programm Beendigung dautert sonst lange




def tokenize(text):
    remove_punct = str.maketrans(string.punctuation, ' ' * len(string.punctuation))
    remove_digits = str.maketrans('', '', string.digits)

    text = text.replace('\n', ' ').replace('\r', '').translate(remove_digits).translate(remove_punct).strip().split()
    return text




if __name__ == '__main__':
    main()