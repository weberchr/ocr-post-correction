import os
import datetime

from utilities import tokenize, dump_dict, ngram_to_simple_dict, progress, get_ngrams


def main():
    starttime = datetime.datetime.now()

    rootdir = 'data/dta/dta_komplett_txt'

    maindict = {}

    fileList = os.listdir(rootdir)
    fileList = ['/'.join([rootdir, fileList[i]]) for i in range(len(fileList))]
    file_count = len(fileList)



    nList = [1, 2, 3]

    for n in nList:
        progress(0, file_count, str(n) + '-grams')

        outdir100k = 'data/ngramme/top100k/' + str(n) + 'gramsTop100k'

        outdir = 'data/ngramme/words/' + str(n) + 'gramsFull'

        Ndir = 'data/ngramme/words/' + str(n) + 'N.txt'


        #total Anzahl an Bigrammen
        N = 0
        C = 0


        #Alle Files durchgehen
        for i, file in enumerate(fileList):

            #Text als Liste von Wörtern speichern
            tokenList = []
            with open(file, encoding="utf-8") as f:
                for line in f:
                    tokenList.extend(tokenize(line))

            if n == 1:
                ngrams = tokenList

            else:
                # Ngramme aus Text erstellen
                ngrams = get_ngrams(tokenList, n)


            #Alle Ngramme durchgehen
            for token in ngrams:
                maindict, N, C = ngram_to_simple_dict(token, maindict, N, C)

            progress(i + 1, file_count, str(n) + '-grams')


        #Abspeichern des Dict
        dump_dict(maindict, outdir)
        #Abspeichern von N
        with open(Ndir, 'w') as f:
            if n == 1:
                N = sum(maindict.values())
                C = len(maindict.keys())

            f.write(str(N))
            f.write('\n')
            f.write(str(C))


        topSet = set(sorted(maindict.keys(), reverse=True, key=lambda x: maindict[x])[:100000])
        maindict = {}
        dump_dict(topSet, outdir100k)




    #print program runtime
    endtime = datetime.datetime.now()
    print("\n", "Completed in", endtime - starttime)
    os._exit(0)  # Um Garbage Collector zu umgehen, Programm Beendigung dautert sonst lange







if __name__ == '__main__':
    main()
