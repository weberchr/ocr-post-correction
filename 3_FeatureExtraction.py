import datetime
import sys
import os
import csv
import pickle
import re
import decimal
from collections import Counter

from utilities import progress


'''
aufruf übers Terminal

python 3_FeatureExtraction.py data/exp/test/f1 data/features test 500
'''

def main(argv):

    rootdir = argv[1]
    try:
        fileList = os.listdir(rootdir)
        fileList = ['/'.join([rootdir, fileList[i]]) for i in range(len(fileList))]

        file_count = len(fileList)
    except:
        print("Fehlerhafte Pfadangabe")
        os._exit(0)


    out = argv[2]
    ende = argv[3]
    outdir = out + '/' + ende + '.csv'

    ngramdir = 'data/ngramme'

    patternBool = False

    maxKVS = int(argv[4])


    patterndir = 'data/patterns.txt'
    patternList = set()

    with open(patterndir, encoding="utf-8") as f:
        for line in f:
            line = line.strip('\n')
            patternList.add(line)

    N1dir = ngramdir + '/words/1N.txt'
    unigramsdir = ngramdir + '/words/1gramsFull.pkl'
    bigramsdir = ngramdir + '/words/2gramsFull.pkl'
    trigramsdir = ngramdir + '/words/3gramsFull.pkl'
    MostCommonDir = ngramdir + '/top100k/'

    # Einlesen der Häufigkeiten
    with open(N1dir, encoding="utf-8") as f:
        lines = f.readlines()
        N1 = int(lines[0])
        V = int(lines[1])




    # Einlesen der 100k häufigsten N-Gramme
    print('lese häufigste N-Gramme')
    with open(MostCommonDir + '1gramsTop100k.pkl', 'rb') as handle:
        topUniSet = set(pickle.load(handle))
    with open(MostCommonDir + '2gramsTop100k.pkl', 'rb') as handle:
        topBiSet = set(pickle.load(handle))
    with open(MostCommonDir + '3gramsTop100k.pkl', 'rb') as handle:
        topTriSet = set(pickle.load(handle))


    # Einlesen der Char Dictionaries
    print('lese Buchstaben N-Gramme')
    with open('data/ngramme/chars/2Gram-char-freq.pkl', 'rb') as handle:
        biChardict = Counter(pickle.load(handle))
    with open('data/ngramme/chars/3Gram-char-freq.pkl', 'rb') as handle:
        triChardict = Counter(pickle.load(handle))


    # Einlesen der Wörter Dictionaries
    print('lese Wörter N-Gramme')
    with open(unigramsdir, 'rb') as handle:
        unigramsdict = pickle.load(handle)
    with open(bigramsdir, 'rb') as handle:
        bigramsdict = pickle.load(handle)
    with open(trigramsdir, 'rb') as handle:
        trigramsdict = pickle.load(handle)




    # Erstellen der Featureliste
    print("reading features")
    starttime = datetime.datetime.now()

    statisticDict = {'normalY1': 0, 'normalY0': 0, 'zusatzY1': 0, 'zusatzY0': 0}

    print("creating csv")

    csvfile = open(outdir, 'a', encoding='utf8')

    #Standard Features
    fieldnames = ['GroundTruthY']
    profilerProbs = ['voteWeight', 'levDistance']
    patternProps = ['countHistPattern', 'countOcrPattern']
    additional = ['isOCR', 'uppercase', 'wordlength']
    wordNgramP = ['tokFreq', 'biProb', 'triProb',
                  'biMLE', 'triMLE',
                  'inTopU100k', 'inTopB100k', 'inTopT100k']

    charNgramP = ['bestCharP', 'worstCharP']


    charSet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
               't', 'u', 'v', 'w', 'x', 'y', 'z', 'ß', 'à', 'ä', 'æ', 'è', 'é', 'ö', 'ü', 'ſ', 'ͤ', 'ꝛ'}

    usedDictList = ['dict_modern_hypothetic_errors', 'dict_guikorpus_exact',
                    'dict_lextraktor_exact', 'dict_modern_exact']




    fieldnames.extend(profilerProbs)
    fieldnames.extend(patternProps)

    fieldnames.extend(additional)
    fieldnames.extend(usedDictList)



    fieldnames.extend(wordNgramP)
    fieldnames.extend(charNgramP)
    #fieldnames.extend(charSet)
    if patternBool:
        fieldnames.extend(patternList)



    writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n', fieldnames=fieldnames)


    fileEmpty = os.stat(outdir).st_size == 0
    if fileEmpty:
        writer.writeheader()

    #print(fieldnames)

    progress(0, file_count, 'Creating Features')
    for i, file in enumerate(fileList):

        with open(file, 'r', encoding="utf-8") as f:

            ocrTrigram = ['', '', '']  # trigram = [vorletztes Token, letztes Token, aktuelles Token]
            ocrToken = ''
            gtToken = ''

            tailDict = {}

            for line in f:
                # head besteht aus @OCR-Token:GT-Token
                head = re.match(r"@(?P<ocrToken>.+):(?P<gtToken>.+)", line)

                # tail ist alles unterhalb eines heads
                tail = re.match(r"(?P<Korrekturvorschlag>.+):{.+\+\["
                                r"(?P<histPatterns>.*)\]}\+ocr\["
                                r"(?P<ocrPatterns>.*)\],voteWeight="
                                r"(?P<voteWeight>.*),levDistance="
                                r"(?P<levDistance>.*),dict="
                                r"(?P<usedDict>.*)", line)


                if head:
                    # KVS in csv schreiben
                    if bool(tailDict):
                        for j, token in enumerate(tailDict.keys()):
                            if j < maxKVS:
                                row = tailDict[token]
                                writer.writerow(row)
                            if j >= maxKVS and ((token == ocrToken) or (token == gtToken)):
                                row = tailDict[token]
                                writer.writerow(row)

                    # wenn OCR Vorschlag nicht in KVS dann diesen ebenfalls in csv schreiben
                    if bool(tailDict) and (ocrToken not in tailDict.keys()) and len(ocrToken) > 3:
                        row = {}
                        if ocrToken == gtToken:
                            statisticDict['zusatzY1'] += 1
                        else:
                            statisticDict['zusatzY0'] += 1
                        row['isOCR'] = 1
                        row = get_row(ocrToken, gtToken, row, ocrTrigram, charSet,
                                      unigramsdict, bigramsdict, trigramsdict,
                                      topUniSet, topBiSet, topTriSet,
                                      biChardict, triChardict, N1, V)
                        writer.writerow(row)


                    tailDict = {}

                    #aktuelle Token aus der group lesen
                    ocrToken = head.group('ocrToken')
                    gtToken = head.group('gtToken')
                    ocrTrigram = (ocrTrigram[1], ocrTrigram[2], ocrToken.lower())



                if tail:
                    KVS = tail.group('Korrekturvorschlag')

                    row = {}

                    if KVS == gtToken:
                        statisticDict['normalY1'] += 1
                    else:
                        statisticDict['normalY0'] += 1

                    if KVS == ocrToken:
                        row['isOCR'] = 1

                    histPatterns = tail.group('histPatterns')
                    # histPositionList = re.findall(r'\d+', histPatterns)
                    histPatterns = re.sub(',\d+', '', histPatterns)
                    histPatternList = re.findall(r'\(.*?\)', histPatterns)
                    histPatternList = [elem.strip('()') for elem in histPatternList]
                    histLen = len(histPatternList)

                    if histLen > 0:
                        row['countHistPattern'] = histLen
                    if patternBool:
                        for pattern in histPatternList:
                            if pattern in patternList:
                                row[pattern] = 1

                    ocrPatterns = tail.group('ocrPatterns')
                    # ocrPositionList = re.findall(r'\d+', ocrPatterns)
                    ocrPatterns = re.sub(',\d+', '', ocrPatterns)
                    ocrPatternList = re.findall(r'\(.*?\)', ocrPatterns)
                    ocrPatternList = [elem.strip('()') for elem in ocrPatternList]
                    ocrLen = len(ocrPatternList)

                    if ocrLen > 0:
                        row['countOcrPattern'] = ocrLen

                    voteWeight = tail.group('voteWeight')
                    row['voteWeight'] = voteWeight

                    levDistance = int(tail.group('levDistance'))
                    if levDistance > 0:
                        row['levDistance'] = levDistance

                    usedDict = tail.group('usedDict')
                    row[usedDict] = 1



                    row = get_row(KVS, gtToken, row, ocrTrigram, charSet,
                                  unigramsdict, bigramsdict, trigramsdict,
                                  topUniSet, topBiSet, topTriSet,
                                  biChardict, triChardict, N1, V)


                    tailDict[KVS] = row


            #letztes token schreiben
            # KVS in csv schreiben
            if bool(tailDict):
                for j, token in enumerate(tailDict.keys()):
                    if j < maxKVS:
                        row = tailDict[token]
                        writer.writerow(row)
                    if j >= maxKVS and ((token == ocrToken) or (token == gtToken)):
                        row = tailDict[token]
                        writer.writerow(row)

            # wenn OCR Vorschlag nicht in KVS dann diesen ebenfalls in csv schreiben
            if bool(tailDict) and (ocrToken not in tailDict.keys()) and len(ocrToken) > 3:
                row = {}
                if ocrToken == gtToken:
                    statisticDict['zusatzY1'] += 1
                else:
                    statisticDict['zusatzY0'] += 1
                row['isOCR'] = 1
                row = get_row(ocrToken, gtToken, row, ocrTrigram, charSet,
                              unigramsdict, bigramsdict, trigramsdict,
                              topUniSet, topBiSet, topTriSet,
                              biChardict, triChardict, N1, V)
                writer.writerow(row)



        progress(i + 1, file_count, 'Creating Features')

    csvfile.close()

    with open('data/eval/statistic.txt', 'a+') as f:
        f.write(ende + str(maxKVS))
        f.write('\n')
        for elem in statisticDict:
            f.write(elem + ': ' + str(statisticDict[elem]))
            f.write('\n')

    # print program runtime
    endtime = datetime.datetime.now()
    print("features completed in", endtime - starttime)

    os._exit(0)  # Um Garbage Collector zu umgehen, Programm Beendigung dautert sonst lange


def read_dicts(dictdir):
    mdict = {}

    ABCD, EFGH, IJKL, MNOP, QRST, UVWX, YZ_ = 'abcd', 'efgh', 'ijkl', 'mnop', 'qrst', 'uvwx', 'yz'
    letter_list = [ABCD, EFGH, IJKL, MNOP, QRST, UVWX, YZ_]
    letters_len = len(letter_list)

    print('reading trigram dictionaries from:', dictdir)

    progress(0, letters_len, 'Dict Reading')

    for i, dic in enumerate(letter_list):
        ddir = os.path.join(dictdir, letter_list[i]) + '.pkl'
        with open(ddir, 'rb') as handle:
            mdict.update(pickle.load(handle))
        progress(i + 1, letters_len, 'Dict Reading')

    return mdict



def get_row(token, gtToken, row, ocrTrigram, charSet,
            unigramsdict, bigramsdict, trigramsdict,
            topUniSet, topBiSet, topTriSet,
            biChardict, triChardict,
            N1, V):

    if token == gtToken:
        row['GroundTruthY'] = 1


    if token[0].isupper():
        row['uppercase'] = 1

    token = token.lower()

    wordlen = len(token)
    row['wordlength'] = wordlen

    # Häufigkeiten aus dem DTA Korpus
    tokCount = 0        #Häufigkeit des KVS
    biCount = 0         #Häufigkeit des 2-Grams
    triCount = 0        #Häufigkeit des 3-Grams
    preTokCount = 0     #Häufigkeit des token vor KVS
    pre2ToksCount = 0   #Häufigkeit der beiden letzten Token vor KVS

    bigram = (ocrTrigram[1], token)
    trigram = (ocrTrigram[0], ocrTrigram[1], token)

    preTok = trigram[1]
    pre2Toks = (trigram[0], trigram[1])


    # Für Unigramme Rel. Häufigkeit zum Vokabular
    if token in unigramsdict:
        tokCount = int(unigramsdict[token])

    if preTok in unigramsdict:
        preTokCount = int(unigramsdict[preTok])

    if bigram in bigramsdict:
        biCount = int(bigramsdict[bigram])


    if pre2Toks in bigramsdict:
        pre2ToksCount = int(bigramsdict[pre2Toks])

    if trigram in trigramsdict:
        triCount = int(trigramsdict[trigram])

    # p1 mit +1 Parameterglättung
    p1 = (tokCount + 1) / (N1 + V)
    p1 = '%.6e' % decimal.Decimal(p1)

    p2MLE = 0
    if preTokCount > 0:
        p2MLE = biCount / preTokCount
    p2 = (biCount + 1) / (preTokCount + V)
    p2 = '%.6e' % decimal.Decimal(p2)

    p3MLE = 0
    if pre2ToksCount > 0:
        p3MLE = triCount / pre2ToksCount
    p3 = (triCount + 1) / (pre2ToksCount + V)
    p3 = '%.6e' % decimal.Decimal(p3)

    if token in topUniSet:
        row['inTopU100k'] = 1
    if bigram in topBiSet:
        row['inTopB100k'] = 1
    if trigram in topTriSet:
        row['inTopT100k'] = 1

    r = {'tokFreq': p1, 'biProb': p2, 'triProb': p3,
         'biMLE': p2MLE, 'triMLE': p3MLE}

    for k, v in r.items():
        if v != 0:
            row[k] = v


    chargramlen = 3
    charNgrams = [token[i:i + chargramlen] for i in range(len(token) - chargramlen + 1)]
    highprob = 0
    lowprob = 1

    for char in charNgrams:
        pre = char[0:2]
        if biChardict[pre] == 0:
            p = 0
        else:
            p = triChardict[char] / biChardict[pre]
        if p > highprob:
            highprob = p
        if p < lowprob:
            lowprob = p

    row['bestCharP'] = highprob
    row['worstCharP'] = lowprob


    # charList = list(token)
    #
    # charDict = {}
    # for char in charList:
    #     if char in charDict:
    #         charDict[char] += 1
    #     else:
    #         charDict[char] = 1
    #
    # for char in charDict.keys():
    #     if char in charSet:
    #         row[char] = charDict[char] / wordlen


    return row


if __name__ == '__main__':
    main(sys.argv)
