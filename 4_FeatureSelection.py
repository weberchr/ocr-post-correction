import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression

plt.rc("font", size=14)
sns.set(style="whitegrid", color_codes=True)



data = pd.read_csv('data/features/dev.csv', header=0)

print(data.shape)


null_columns = data.columns[data.isnull().all()]
print(data.isnull().any(axis=1)[null_columns].head())

data = data.dropna(axis=1, how='all')
data = data.fillna(0)

print(data.shape)





print('----------------')
print('Feature Selection with Recursive Feature Elimination (RFE)')

y = data['GroundTruthY']
X = data.drop(['GroundTruthY'], axis=1)

features = X.columns.values
print(features)

logreg = LogisticRegression()
rfe = RFE(logreg, 18)

rfe = rfe.fit(X, y)

support = rfe.support_
print(support)
ranking = rfe.ranking_
print(ranking)


final_features = []
for i, val in enumerate(ranking):
    if val < 5:
        final_features.append(features[i])

with open('data/eval/BesteFeatures.txt', 'w', encoding="utf-8") as f:
    for i, val in enumerate(ranking):
        f.write(str(features[i]) + ' : ' + str(val))
        f.write('\n')
print('----------------')

