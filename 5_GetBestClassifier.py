import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.externals import joblib



devDir = 'data/features/devSmall.csv'
trainDir = 'data/features/train.csv'
testDir = 'data/features/testSmall.csv'


print('lese CSV-Daten...')
devData = pd.read_csv(devDir, header=0)
trainData = pd.read_csv(trainDir, header=0)
testData = pd.read_csv(testDir, header=0)


droplist = ['tokFreq', 'biProb', 'triProb']

devData = devData.drop(droplist, axis=1)
trainData = trainData.drop(droplist, axis=1)
testData = testData.drop(droplist, axis=1)


devData = devData.fillna(0)
trainData = trainData.fillna(0)
testData = testData.fillna(0)



y_test = testData['GroundTruthY']
X_test = testData.drop(['GroundTruthY'], axis=1)

y_dev = devData['GroundTruthY']
X_dev = devData.drop(['GroundTruthY'], axis=1)

y_train = trainData['GroundTruthY']
X_train = trainData.drop(['GroundTruthY'], axis=1)




cList = [0.001, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100, 1000]
penaltyList = ['l1', 'l2']
weightList = [None, 'balanced']

best_dev_acc = 0
best_classifier = None
best_classifier_name = None

print('finde beste Hyperparameter...\n')

for penalty in penaltyList:
    for weight in weightList:
        for c in cList:

            # Fit to data and predict using pipelined GNB and PCA.
            unscaled_clf = LogisticRegression(C=c, penalty=penalty, class_weight=weight)
            unscaled_clf.fit(X_train, y_train)

            # Fit to data and predict
            std_clf = make_pipeline(StandardScaler(), LogisticRegression(C=c, penalty=penalty, class_weight=weight))
            std_clf.fit(X_train, y_train)

            # Extract logit and scaler from pipeline
            logit_std = std_clf.named_steps['logisticregression']
            scaler = std_clf.named_steps['standardscaler']

            #X_dev_std = logit_std.fit(scaler.transform(X_dev), y_dev)


            dev_accuracy_unscaled = accuracy_score(y_dev, unscaled_clf.predict(X_dev))
            dev_accuracy_scaled = accuracy_score(y_dev, std_clf.predict(X_dev))

            Acc = dev_accuracy_unscaled
            scale = 'Unscaled'
            better_cl = unscaled_clf
            if dev_accuracy_scaled > dev_accuracy_unscaled:
                scale = 'Scaled'
                Acc = dev_accuracy_scaled
                better_cl = std_clf


            cl_name = scale + ' LogisticRegression(C=' + str(c) + ', penalty=\'' + str(penalty) \
                      + '\', class_weight=' + str(weight) + ')'

            print("Classifier: %s  -  Development Accuracy: %.4f" % (cl_name, Acc))

            if Acc > best_dev_acc:
                best_dev_acc = Acc
                best_classifier = better_cl
                best_classifier_name = cl_name


pred_Y = best_classifier.predict(X_test)
test_accuracy = accuracy_score(y_test, pred_Y)
print("\nBest classifier:\n%s" % best_classifier_name)
print("\nDev  Accuracy: %.4f" % best_dev_acc)
print("Test Accuracy: %.4f\n" % test_accuracy)

print(classification_report(y_test, pred_Y))



print('Besten Classifier abspeichern...')
joblib.dump(best_classifier, 'data/classifier/logitBest.pkl', compress=9)

with open('data/classifier/features-logitBest.txt', 'w', encoding="utf-8") as f:
    features = list(X_dev.columns.values)
    f.write(str(features))