import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler





trainDir = 'data/features/train.csv'
testDir = 'data/features/testSmall.csv'


print('lese CSV-Daten...')
trainData = pd.read_csv(trainDir, header=0)
testData = pd.read_csv(testDir, header=0)

#1 Beste Features nach RFE
droplist1 = ['tokFreq', 'biProb', 'triProb']

#2 nur Basis Profiler Features testen
droplist2 = ['tokFreq', 'biProb', 'triProb', 'biMLE', 'triMLE', 'inTopU100k', 'inTopB100k', 'inTopT100k', 'bestCharP', 'worstCharP']

#3 nur Basis & bedingte P und rel. Häufigkeit testen
droplist3 = ['biMLE', 'triMLE', 'inTopU100k', 'inTopB100k', 'inTopT100k', 'bestCharP', 'worstCharP']

#4 nur Basis & MLE testen
droplist4 = ['tokFreq', 'biProb', 'triProb', 'inTopU100k', 'inTopB100k', 'inTopT100k', 'bestCharP', 'worstCharP']

#5 nur Basis & inTop100k Features testen
droplist5 = ['tokFreq', 'biProb', 'triProb', 'biMLE', 'triMLE', 'bestCharP', 'worstCharP']

#6 nur Basis & Buchstaben Features testen
droplist6 = ['tokFreq', 'biProb', 'triProb', 'biMLE', 'triMLE', 'inTopU100k', 'inTopB100k', 'inTopT100k']


#7 Basis und RFE optimierte Features ohne CharFeatures
droplist7 = ['tokFreq', 'biProb', 'triProb', 'bestCharP', 'worstCharP']




droplist = droplist7



trainData2 = trainData.drop(droplist, axis=1)
testData2 = testData.drop(droplist, axis=1)


trainData2 = trainData2.fillna(0)
testData2 = testData2.fillna(0)

y_test = testData2['GroundTruthY']
X_test = testData2.drop(['GroundTruthY'], axis=1)


y_train = trainData2['GroundTruthY']
X_train = trainData2.drop(['GroundTruthY'], axis=1)


classifier = make_pipeline(StandardScaler(), LogisticRegression(C=0.5, penalty='l1'))
classifier.fit(X_train, y_train)
y_pred = classifier.predict(X_test)

print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(classifier.score(X_test, y_test)))


print('---')
print('Confusion Matrix')
'''
[[509246     34]
 [    67    226]]

true negatives      false negatives
true positives      false positives

The result is telling us that we have
509246+226 correct predictions and
67+34 incorrect predictions.
'''


confusion_matrix = confusion_matrix(y_test, y_pred)
print(confusion_matrix)


print('---')
print('Compute precision, recall, F-measure and support')
'''
The support is the number of occurrences of each class in y_test.
'''

print(classification_report(y_test, y_pred))
