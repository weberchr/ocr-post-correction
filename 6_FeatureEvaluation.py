import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns



plt.rc("font", size=14)
sns.set(style="whitegrid", color_codes=True)




data = pd.read_csv('data/features/test.csv', header=0)

print(data.shape)
print(list(data.columns))


data = data.fillna(0)




meandf = data.groupby('GroundTruthY').mean()
print(meandf)
for line in meandf:
    print(meandf[line])




sns.countplot(x='GroundTruthY', data=data, palette='hls')
plt.savefig('data/eval/x&y')
plt.show()



table = pd.crosstab(data.levDistance, data.GroundTruthY)
table.div(table.sum(1).astype(float), axis=0).plot(kind='bar', stacked=True)
plt.xlabel('levDistance')
plt.ylabel('GroundTruthY')
plt.savefig('data/eval/levDistance')
plt.show()

