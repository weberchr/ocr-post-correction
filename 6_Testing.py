import matplotlib.pyplot as plt
import pandas as pd
from sklearn.externals import joblib
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

classifier = joblib.load('data/classifier/logitBest.pkl')

with open('data/classifier/features-logitBest.txt', encoding="utf-8") as f:
    cl_features = f.readline()


print('reading test data...')
data = pd.read_csv('data/features/testSmall.csv', header=0)
droplist = ['tokFreq', 'biProb', 'triProb']
data = data.drop(droplist, axis=1)
data = data.fillna(0)

y_test = data['GroundTruthY']
X_test = data.drop(['GroundTruthY'], axis=1)



y_pred = classifier.predict(X_test)

print('Accuracy auf Testset: {:.2f}'.format(classifier.score(X_test, y_test)))




print('Confusion Matrix')

confusion_matrix = confusion_matrix(y_test, y_pred)
print(confusion_matrix)

print(classification_report(y_test, y_pred))


