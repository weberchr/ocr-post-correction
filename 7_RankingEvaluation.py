import datetime
import os
import pickle
import re
from collections import Counter
from collections import OrderedDict
from sklearn.externals import joblib

from utilities import progress


'''
hinweise:
'''

def main():

    rootdirList = ['data/out', 'data/exp/test/f1', 'data/exp/test/f2']

    overstats = []

    for rootdir in rootdirList:

        fileList = os.listdir(rootdir)
        fileList = ['/'.join([rootdir, fileList[i]]) for i in range(len(fileList))]


        maxKVS = 5

        total = 0
        OCRmitKVS = 0
        ersterKVSistGT = 0
        KVSinVorschlaegen = 0
        KVSnichtInVorschlaegen = 0

        for file in fileList:
            with open(file, 'r', encoding="utf-8") as f:

                ocrToken = ''
                gtToken = ''
                KVSList = []

                for line in f:
                    # head besteht aus @OCR-Token:GT-Token
                    head = re.match(r"@(?P<ocrToken>.+):(?P<gtToken>.+)", line)
                    # tail ist alles unterhalb eines heads

                    if rootdir is 'data/exp/test/f1' or rootdir is 'data/exp/test/f2':
                        tail = re.match(r"(?P<Korrekturvorschlag>.+):{.+\+\["
                                            r"(?P<histPatterns>.*)\]}\+ocr\["
                                            r"(?P<ocrPatterns>.*)\],voteWeight="
                                            r"(?P<voteWeight>.*),levDistance="
                                            r"(?P<levDistance>.*),dict="
                                            r"(?P<usedDict>.*)", line)
                    if rootdir is 'data/out':
                        tail = re.match(r"(?P<Korrekturvorschlag>.+):(?P<weight>\d.*)", line)

                    if head:
                        total += 1
                        if KVSList:
                            OCRmitKVS += 1
                            if gtToken == KVSList[0]:
                                ersterKVSistGT += 1
                            elif gtToken in KVSList:
                                KVSinVorschlaegen += 1
                            else:
                                KVSnichtInVorschlaegen += 1

                        ocrToken = head.group('ocrToken')
                        gtToken = head.group('gtToken')
                        KVSList = []


                    if tail:
                        if len(KVSList) <= maxKVS:
                            KVS = tail.group('Korrekturvorschlag')
                            KVSList.append(KVS)

        overstats.append([total, OCRmitKVS, ersterKVSistGT, KVSinVorschlaegen, KVSnichtInVorschlaegen])


    rerankedstats = overstats[0]
    profilerstats = overstats[1:]

    totalprofilerstats = []

    for i, elem in enumerate(profilerstats[0]):
        e = elem + profilerstats[1][i]
        totalprofilerstats.append(e)



    vergleich = [totalprofilerstats, rerankedstats]
    überschrift = ['Proflier Daten', 'Nach Neu-Ranking mit Logit Klassifikator']
    for i, l in enumerate(vergleich):
        print(überschrift[i])
        y1 = (l[2]*100)/l[1]
        y0 = ((l[1]-l[2])*100)/l[1]
        y1Notinlist = (l[4]*100)/l[1]

        print('Erster Kandidat richtig: %.2f' % y1)
        print('Erster Kandidat falsch: %.2f' % y0)
        print('Richtiger Kandidat nicht in den ersten Vorschlägenen: %.2f' % y1Notinlist)
        print()


if __name__ == '__main__':
    main()
