import sys
import numpy as np
import six
import string
import pickle
import matplotlib.pyplot as plt


# return zip von Ngrammen
def get_ngrams(input_list, n):
    return zip(*[input_list[i:] for i in range(n)])


# gibt Liste an tokens zurück
def tokenize(text):
    remove_punct = str.maketrans(string.punctuation, ' ' * len(string.punctuation))
    remove_digits = str.maketrans('', '', string.digits)

    tokenList = text.replace('\n', ' ').replace('\r', '').translate(remove_digits)\
        .translate(remove_punct).lower().strip().split()
    return tokenList


# Abspeichern des Dicts
def dump_dict(d, filedir):
    outdir = filedir + '.pkl'
    with open(outdir, 'wb') as f:
        pickle.dump(d, f, protocol=pickle.HIGHEST_PROTOCOL)



def ngram_to_simple_dict(ngram, dictX, N, C):
    N += 1
    if ngram in dictX:
        dictX[ngram] += 1
    else:
        C += 1
        dictX[ngram] = 1
    return dictX, N, C




'''
Fortschrittsbalken und Grafiken
Code von:
https://stackoverflow.com/questions/3160699/python-progress-bar
'''

def progress (count, total, about):
    bar_len = 60
    prefix = 'Progress:'
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '█' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('%s %s |%s| %s %s\r' % (about, prefix, bar, percents, '%'))

    sys.stdout.flush()

    # Print New Line on Complete
    if count == total:
        print()
